package com.example.alexander.ufgg.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexander.ufgg.R;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/25/16.
 */
public class ImportFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_import,container,false);
        return rootView;
    }
}
