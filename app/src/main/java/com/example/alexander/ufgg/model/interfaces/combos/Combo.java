package com.example.alexander.ufgg.model.interfaces.combos;

import java.io.Serializable;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/20/16.
 */
public interface Combo extends Serializable {
    /**
     * This method returns the Combo ID.
     * @return
     */
    int getComboId();
    /**
     * This method returns the Combo value.
     * @return
     */
    String getCombo();
    /**
     * This method returns the Combo post date in Epoch.
     * @return
     */
    String getPostDate();

    /**
     * This method returns combo damage.
     * @return
     */
    String getDamage();

    /**
     * This method returns combo number of hits in the combo.
     * @return
     */
    String getComboCount();

    /**
     * This method returns number of meter(bars) needed for the combo.
     * @return
     */
    String getMeterNumber();
}
