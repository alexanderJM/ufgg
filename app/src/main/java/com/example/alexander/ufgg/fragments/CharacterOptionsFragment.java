package com.example.alexander.ufgg.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.alexander.ufgg.R;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 10/11/16.
 */
public class CharacterOptionsFragment extends Fragment {

    Button combo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_character_option,container,false);
        combo = (Button)rootView.findViewById(R.id.optionCombos);
        combo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CombosByCharacterFragment combosByCharacterFragment= new CombosByCharacterFragment();
                combosByCharacterFragment.setArguments(getArguments());
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, combosByCharacterFragment, "CommentFragment");
                ft.addToBackStack(null).commit();
            }
        });
        return rootView;
    }
}
