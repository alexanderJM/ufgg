package com.example.alexander.ufgg.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.recycler_view.Footer;
import com.example.alexander.ufgg.utils.Item;
import com.example.alexander.ufgg.utils.OnItemClickListener;
import com.example.alexander.ufgg.utils.OnVolleyResponse;
import com.example.alexander.ufgg.volley_task.VolleyCombos;
import com.example.alexander.ufgg.recycler_view.combo_recycler_view.ComboAdapter;

import java.util.ArrayList;


/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/25/16.
 */
public class MainFragment  extends Fragment {

    RecyclerView recyclerView;
    ComboAdapter adapter;
    View footerView;
    LinearLayoutManager layoutManager;
    ArrayList<Item> arrayList = new ArrayList<>();
    AlertDialog.Builder builder;
    double totalItemCount;
    ImageView imageView;
    Boolean hasMore =true;
    VolleyCombos volleyCombos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main,container,false);
        footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.progress_footer, null, false);
        builder = new AlertDialog.Builder(getActivity());
        recyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        imageView = new ImageView(getActivity());


        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        //arrayList.add(new Footer());
        /*adapter = new ComboAdapter(arrayList,getActivity());
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int pos, Bundle bundle) {
                CommentFragment commentFragment = new CommentFragment();
                commentFragment.setArguments(bundle);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, commentFragment, "CommentFragment");
                ft.addToBackStack(null).commit();
            }
        });
        recyclerView.setAdapter(adapter);*/
        volleyCombos = new VolleyCombos(getActivity());
        VolleyResponse(volleyCombos);
        final SwipeRefreshLayout swipeView = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe);
        volleyCombos = new VolleyCombos(getActivity());
        try {
            arrayList = volleyCombos.getComboListWithPagination(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        swipeView.setColorSchemeColors(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_green_dark);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refresh");
                (new Handler()).post(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        try {
                            arrayList = volleyCombos.getComboListWithPagination(0);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (hasMore && !(hasFooter())) {
                    totalItemCount = layoutManager.getItemCount();
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    if (layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 2) {
                        hasMore = false;
                        Log.v("...", "Last Item Wow !");
                        if (totalItemCount % 10 == 0) {
                            arrayList.add(new Footer());
                            recyclerView.getAdapter().notifyItemInserted(arrayList.size() - 1);
                            volleyCombos = new VolleyCombos(getActivity());
                            try {
                                volleyCombos.getComboListWithPagination((int) totalItemCount / 10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        });
        return rootView;
    }

    private boolean hasFooter() {
        Boolean hasFooter;
        try{

            hasFooter = arrayList.get(arrayList.size() - 1) instanceof Footer;

        }catch (Exception e){
            hasFooter = false;
        }

        return hasFooter;
    }
    private void VolleyResponse(VolleyCombos volleyCombos){

        volleyCombos.setVolleyResponse(new OnVolleyResponse() {
            @Override
            public void onResponse(int position,ArrayList<Item> data) {
                if (position == 0) {
                    adapter = new ComboAdapter(data, getActivity());
                    adapter.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClicked(View view, int pos, Bundle bundle) {
                            CommentFragment commentFragment = new CommentFragment();
                            commentFragment.setArguments(bundle);
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.replace(R.id.content_frame, commentFragment, "CommentFragment");
                            ft.addToBackStack(null).commit();
                        }
                    });
                    recyclerView.setAdapter(adapter);
                    hasMore = true;
                }else{
                    int size = arrayList.size();
                    arrayList.remove(size - 1);//removes footer
                    arrayList.addAll(data);
                    recyclerView.getAdapter().notifyItemRangeChanged(size, arrayList.size() - size);
                    hasMore = true;
                }
            }

            @Override
            public void onFailure() {
                if(arrayList.size()>0) {
                    int size = arrayList.size();
                    arrayList.remove(size - 1);//removes footer
                    recyclerView.getAdapter().notifyItemRangeChanged(size - 1, 1);
                }
            }
        });
    }
}

