package com.example.alexander.ufgg.utils;

import android.support.v7.widget.RecyclerView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/24/16.
 */
public class AnimationUtils {


    public static void animate(RecyclerView.ViewHolder holder,boolean goesDown){

        YoYo.with(Techniques.FadeInLeft).duration(200).playOn(holder.itemView);
        /*ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(holder.itemView, "translationY", goesDown==true?100:-100, 0);

        animatorTranslateY.setDuration(1000);
        animatorTranslateY.start();*/
    }
}
