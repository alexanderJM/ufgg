package com.example.alexander.ufgg.recycler_view.character_recycler_view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.example.alexander.ufgg.R;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/28/16.
 */
public class TextViewHolder extends RecyclerView.ViewHolder {
    public ImageButton imageButton;
    public TextViewHolder(View itemView) {
        super(itemView);
        imageButton = (ImageButton) itemView.findViewById(R.id.character_select_button);
    }
}
