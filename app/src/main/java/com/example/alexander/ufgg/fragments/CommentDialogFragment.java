package com.example.alexander.ufgg.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.model.comment.CommentImpl;
import com.example.alexander.ufgg.model.interfaces.combos.Combo;
import com.example.alexander.ufgg.model.interfaces.comment.Comment;
import com.example.alexander.ufgg.volley_task.VolleyComments;

import org.json.JSONException;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 10/18/16.
 */
public class CommentDialogFragment extends DialogFragment {
    EditText editText;
    Combo comboEntity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        comboEntity = (Combo) getArguments().get("entity");
        LayoutInflater i = getActivity().getLayoutInflater();
        View v = i.inflate(R.layout.fragment_dialog_comment, null);
        editText = (EditText) v.findViewById(R.id.dialog_comment);
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        b.setTitle("Write Comment...");
        b.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Comment comment = new CommentImpl(comboEntity, editText.getText().toString());
                        VolleyComments volleyComments = new VolleyComments(getActivity());
                        try {
                            volleyComments.insertComment(comment, comboEntity.getComboId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
        );
        b.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });


        b.setView(v);
        return b.create();
    }
}
