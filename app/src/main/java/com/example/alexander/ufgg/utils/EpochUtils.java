package com.example.alexander.ufgg.utils;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/28/16.
 */
public class EpochUtils {

    public static String stringEpochToDate(String epoch) {
        DateFormat format = DateFormat.getDateTimeInstance();
        Date expiry = new Date(Long.parseLong(epoch));
        return format.format(expiry);
    }
}
