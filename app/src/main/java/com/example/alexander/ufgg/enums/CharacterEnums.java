package com.example.alexander.ufgg.enums;


import com.example.alexander.ufgg.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/29/16.
 */
public enum CharacterEnums {

    MKX_ALIEN                                          (1,"Alien",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_JASON_VOORHEES                                 (2,"Jason Voorhees",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_KUNG_LAO                                       (3,"Kung Lao",GameEnums.MORTAL_KOMBAT_X, R.drawable.kung_lao),
    MKX_JAX                                            (4,"Jax",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_SONYA                                          (5,"Sonya",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_KENSHI                                         (6,"Kenshi",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_KITANA                                         (7,"Kitana",GameEnums.MORTAL_KOMBAT_X,R.drawable.kitana),
    MKX_SCORPION                                       (8,"Scorpion",GameEnums.MORTAL_KOMBAT_X,R.drawable.scorpion),
    MKX_SUB_ZERO                                       (9,"Sub-Zero",GameEnums.MORTAL_KOMBAT_X,R.drawable.sub_zero),
    MKX_MILEENA                                        (10,"Mileena",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_TAKEDA                                         (11,"Takeda",GameEnums.MORTAL_KOMBAT_X,R.drawable.takeda),
    MKX_CASSIE_CAGE                                    (12,"Cassie Cage",GameEnums.MORTAL_KOMBAT_X,R.drawable.cassie_cage),
    MKX_JACQUI                                         (13,"Jacqui",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_KUNG_JIN                                       (14,"Kung Jin",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_TANYA                                          (15,"Tanya",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_BO_RAI_CHO                                     (16,"Bo'Rai Cho",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_LEATHERFACE                                    (17,"Leatherface",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_PREDATOR                                       (18,"Predator",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_SHINNOK                                        (19,"Shinnok",GameEnums.MORTAL_KOMBAT_X,R.drawable.shinnok),
    MKX_KANO                                           (20,"Kano",GameEnums.MORTAL_KOMBAT_X,R.drawable.kano),
    MKX_JHONNY_CAGE                                    (21,"Johnny Cage",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_ERRON_BLACK                                    (22,"Erron Black",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_LIU_KANG                                       (23,"Liu Kang",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_ERMAC                                          (24,"Ermac",GameEnums.MORTAL_KOMBAT_X,R.drawable.ermac),
    MKX_KOTAL_KAHN                                     (25,"Kotal Kahn",GameEnums.MORTAL_KOMBAT_X,R.drawable.kotal_kahn),
    MKX_REPTILE                                        (26,"Reptile",GameEnums.MORTAL_KOMBAT_X,R.drawable.reptile),
    MKX_FERRA_TORR                                     (27,"Ferra/Torr",GameEnums.MORTAL_KOMBAT_X,R.drawable.ferra_torr),
    MKX_DVORAH                                         (28,"D'Vorah",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_RAIDEN                                         (29,"Raiden",GameEnums.MORTAL_KOMBAT_X,R.drawable.raiden),
    MKX_QUAN_CHI                                       (30,"Quan Chi",GameEnums.MORTAL_KOMBAT_X,R.drawable.quan_chi),
    MKX_GORO                                           (31,"Goro",GameEnums.MORTAL_KOMBAT_X,R.drawable.goro),
    MKX_TREMOR                                         (32,"Tremor",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait),
    MKX_TRIBORG                                        (33,"Triborg",GameEnums.MORTAL_KOMBAT_X,R.drawable.default_potrait);

    private int id;
    private String name;
    private GameEnums gameEnum;
    private int drawable;

    CharacterEnums(int id, String name,GameEnums gameEnum, int drawable) {
        this.id = id;
        this.name = name;
        this.gameEnum = gameEnum;
        this.drawable = drawable;

    }

    public static CharacterEnums getValue(int code) {
        for (CharacterEnums items : CharacterEnums.values()) {
            if (items.id == code) {
                return items;
            }
        }
        return null;
    }

    public static List<CharacterEnums> getEnumsByGame(GameEnums gameEnums){
        List<CharacterEnums> characterByGame = new ArrayList<>();

        for (CharacterEnums items : CharacterEnums.values()) {
            if (items.gameEnum == gameEnums) {
                characterByGame.add(items);
            }
        }
        return characterByGame;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public GameEnums getGameEnum() {
        return gameEnum;
    }

    public int getDrawable() {
        return drawable;
    }
}
