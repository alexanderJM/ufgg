package com.example.alexander.ufgg.utils;

import java.util.HashMap;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/20/16.
 */
public class PairingFunction {

    public int pair(int k1,int k2){
        //Cantor pairing function

        //http://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function
        int z = (int)(0.5 * (k1 + k2) * (k1 + k2 + 1) + k2);
        //if safe and (k1, k2) != depair(z):
        //raise ValueError("{} and {} cannot be paired".format(k1, k2))
        return z;
    }

    public HashMap<String,Integer> depair(int z){

        //Inverse of Cantor pairing function
        //http://en.wikipedia.org/wiki/Pairing_function#Inverting_the_Cantor_pairing_function
        double w = Math.floor((Math.sqrt(8 * z + 1) - 1)/2);
        double t = (Math.pow(w,2) + w) / 2;
        int y = (int)(z - t);
        int x = (int)(w - y);
        HashMap<String,Integer> xy = new HashMap<>();
        xy.put("x", x);
        xy.put("y", y);

        // assert z != pair(x, y, safe=False):
        return xy;
    }


}
