package com.example.alexander.ufgg.utils;

import android.os.Bundle;
import android.view.View;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 10/9/16.
 */
public interface OnItemClickListener {

    void onItemClicked(View view, int pos,Bundle bundle);

}
