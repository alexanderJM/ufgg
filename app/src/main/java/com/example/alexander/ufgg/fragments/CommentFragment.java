package com.example.alexander.ufgg.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.utils.ComboUtils;
import com.example.alexander.ufgg.combo_controls.Control;
import com.example.alexander.ufgg.model.comment.CommentImpl;
import com.example.alexander.ufgg.model.interfaces.combos.Combo;
import com.example.alexander.ufgg.model.interfaces.comment.Comment;
import com.example.alexander.ufgg.recycler_view.comment_recycler_view.CommentAdapter;
import com.example.alexander.ufgg.utils.Item;
import com.example.alexander.ufgg.volley_task.VolleyComments;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/25/16.
 */
public class CommentFragment extends Fragment{

    Combo comboEntity;
    Button send;
    TextView combo,date;
    EditText editText;
    RecyclerView recyclerView;
    CommentAdapter commentAdapter;
    Boolean hasMore =true;
    LinearLayoutManager layoutManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_comment,container,false);
        combo = (TextView)rootView.findViewById(R.id.commentCombo);
        date = (TextView)rootView.findViewById(R.id.commentDate);
        send = (Button)rootView.findViewById(R.id.buttonSend);
        editText = (EditText)rootView.findViewById(R.id.comment_txt);
        comboEntity = (Combo)getArguments().get("entity");
        recyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerView_comment);
        VolleyComments volleyComments = new VolleyComments(getActivity());
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        try {
            volleyComments.getCommentList(comboEntity.getComboId());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int[] comboItems = ComboUtils.convertSplit(comboEntity.getCombo());
        String combo2= comboEntity.getCombo().replace("-", " ")+" ";
        SpannableString spannableString = new SpannableString(combo2);
        int stringPosition = 0;
        for (int i = 0; i < comboItems.length; i++) {
            Drawable d = getActivity().getResources().getDrawable(Control.getValue(comboItems[i]).getPs4Drawable());
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
            spannableString.setSpan(span, stringPosition, stringPosition+String.valueOf(comboItems[i]).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            stringPosition = stringPosition+String.valueOf(comboItems[i]).length()+1;
        }
        combo.setText(spannableString);
        date.setText("Date IS: " + comboEntity.getPostDate());

        volleyComments.setCallBackVolley(new VolleyComments.CallBackVolley() {
            @Override
            public void onSuccessGetCommentList(ArrayList<Item> ArrayList) {
                commentAdapter = new CommentAdapter(ArrayList,getActivity());
                recyclerView.setAdapter(commentAdapter);
                hasMore = true;
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                CommentDialogFragment dialogFragment = new CommentDialogFragment ();
                dialogFragment.setArguments(getArguments());
                dialogFragment.show(fm, "Sample Fragment");

                /*FragmentManager ft = getFragmentManager();

                FragmentTransaction fragmentTransaction = ft.beginTransaction();

                fragmentTransaction.replace(R.id.content_frame, new CommentDialogFragment(), "CommentDialogFragment");

                fragmentTransaction.addToBackStack(null).commit();*/

                //Comment comment = new CommentImpl(comboEntity,editText.getText().toString());
                /*VolleyComments volleyComments = new VolleyComments(getActivity());
                try {
                    volleyComments.insertComment(comment, comboEntity.getComboId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }
        });
        return rootView;
    }

}