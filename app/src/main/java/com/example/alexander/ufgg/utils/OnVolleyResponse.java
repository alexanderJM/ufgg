package com.example.alexander.ufgg.utils;

import java.util.ArrayList;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 10/9/16.
 */
public interface OnVolleyResponse {
    void onResponse(int position,ArrayList<Item> data);
    void onFailure();
}
