package com.example.alexander.ufgg.recycler_view.combo_recycler_view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.alexander.ufgg.utils.ComboUtils;
import com.example.alexander.ufgg.combo_controls.Control;
import com.example.alexander.ufgg.model.combos.ComboImp;
import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.model.interfaces.combos.Combo;
import com.example.alexander.ufgg.recycler_view.Footer;
import com.example.alexander.ufgg.utils.AnimationUtils;
import com.example.alexander.ufgg.utils.EpochUtils;
import com.example.alexander.ufgg.utils.Item;
import com.example.alexander.ufgg.utils.OnItemClickListener;

import java.util.List;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/21/16.
 */
public class ComboAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static OnItemClickListener mOnItemClickLister;

    public void setOnItemClickListener(OnItemClickListener listener) {

        mOnItemClickLister = listener;
    }
    private List<Item> data;

    private static final int TYPE_COMBO = 0;
    private static final int TYPE_FOOTER = 1;
    private Context mContext;
    public ComboAdapter(@NonNull List<Item> data,Context mContext) {
        this.data = data;
        this.mContext =mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof ComboImp) {
            return TYPE_COMBO;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_COMBO) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
            data.get(0);
            ComboViewHolder cvh = new ComboViewHolder(row);
            return cvh;
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_footer, parent, false);
            FooterViewHolder vh = new FooterViewHolder(row);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ComboViewHolder) {
            ComboViewHolder comboViewHolder = (ComboViewHolder)holder;
            comboViewHolder.getCombo().setText("");
            comboViewHolder.getPostDate().setText("");
            Combo comboEntity = (ComboImp) data.get(position);
            comboViewHolder.setComboEntity(comboEntity);
            comboViewHolder.getPostDate().setText(EpochUtils.stringEpochToDate(comboEntity.getPostDate()));

            int[] comboItems = ComboUtils.convertSplit(comboEntity.getCombo());
            String combo= comboEntity.getCombo().replace("-", " ")+" ";
            SpannableString spannableString = new SpannableString(combo);
            int stringPosition = 0;
            for (int i = 0; i < comboItems.length; i++) {
                Drawable d = mContext.getResources().getDrawable(Control.getValue(comboItems[i]).getPs4Drawable());
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                spannableString.setSpan(span, stringPosition, stringPosition+String.valueOf(comboItems[i]).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                stringPosition = stringPosition+String.valueOf(comboItems[i]).length()+1;
            }
            comboViewHolder.getCombo().setText(spannableString);
            AnimationUtils.animate(holder, false);

        }
        //FOOTER: nothing to do

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ComboViewHolder extends RecyclerView.ViewHolder {
        private TextView combo, postDate;
        private CardView cardView;
        private Combo comboEntity;
        public ComboViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);

            combo = (TextView) itemView.findViewById(R.id.combo);
            postDate = (TextView)itemView.findViewById(R.id.date);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("entity", comboEntity);
                    int position = v.getLayoutDirection();
                    mOnItemClickLister.onItemClicked(v, position,bundle);
                }
            });
        }

        public void setComboEntity(Combo combo2) {
            this.comboEntity = combo2;
        }

        public TextView getCombo() {
            return combo;
        }

        public TextView getPostDate() {
            return postDate;
        }

        public CardView getCardView() {
            return cardView;
        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar getProgressBar() {
            return progressBar;
        }

        private ProgressBar progressBar;

        public FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.footer);
        }
    }

}
