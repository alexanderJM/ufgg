package com.example.alexander.ufgg.combo_controls;

import com.example.alexander.ufgg.R;

/**
 * Created by Alexander Jimenez (alex_jimenez76@hotmail.com) on 8/18/16.
 */
public enum Control {

    UP                                          (0,"u","Up key", R.drawable.ps4_up_30x30),
    DOWN                                        (1,"d","Down key",R.drawable.ps4_down_30x30),
    FORWARD                                     (2,"f","Forward key",R.drawable.ps4_forward_30x30),
    BACK                                        (3,"b","Back key",R.drawable.ps4_back_30x30),
    ONE                                         (4,"fp/lp","Front Punch (fp)/Light Punch (lp): square on PS4, X on Xbox-one",R.drawable.ps4_square_30x30),
    TWO                                         (5,"bp/mp","Back Punch (bp)/Medium Punch (lp): triangle on PS4, Y on Xbox-one",R.drawable.ps4_triangle_30x30),
    THREE                                       (6,"fk/lk","Front kick (fk)/Light Kick (lp): X on PS3, A on Xbox-one",R.drawable.ps4_x_30x30),
    FOUR                                        (7,"bk/mk","back kick (bk)/Light Kick (lp): Circle on PS4, B on Xbox-one",R.drawable.ps4_o_30x30),
    JUMP                                        (8,"j","Jump Key",8),
    JUMP_IN                                     (9,"ji","Jump Towards Opponent",9),
    NEUTRAL_JUMP                                (10,"nj","Jump directly up",10),
    FIRST_CANCEL                                (11,"nj","Jump directly up",11),
    CANCEL                                      (12,"xx","Cancel Current move",12),
    EX                                          (13,"ex","indicates EX version of a special",13),
    METER_BURN                                  (14,"MB","indicates a MB version of a special/move",14),
    RUN                                         (15,"run","indicates a run",15),
    THROW                                       (16,"throw","indicates a throw",16),
    END_CURRENT_STRING                          (17,",","end of string/normal/special",17),
    PLUS                                        (18,"+","indicates button pressed at same time",18),
    STANDING                                    (19,"s","indicates the player must be standing",19),
    CROUCHING                                   (20,"c","indicates the player must be crouching",20),
    AIR_DASH                                    (21,"air dash","A dash performed in the air",21),
    DIZZY                                       (22,"dizzy","When a character is hit too much in a short period of time",22);

    private int buttonValue;
    private String diminutive;
    private String description;
    private int ps4Drawable;

    Control(int buttonValue, String diminutive, String description, int ps4Drawable) {
        this.buttonValue = buttonValue;
        this.diminutive = diminutive;
        this.description = description;
        this.ps4Drawable = ps4Drawable;

    }

    public static Control getValue(int code) {
        for (Control items : Control.values()) {
            if (items.buttonValue == code) {
                return items;
            }
        }
        return null;
    }
    public int getButtonValue() {
        return buttonValue;
    }
    public String getdiminutive() {
        return diminutive;
    }
    public String getDescription() {
        return description;
    }

    public int getPs4Drawable() {
        return ps4Drawable;
    }

    public String getDiminutive() {
        return diminutive;
    }
}


