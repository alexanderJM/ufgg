package com.example.alexander.ufgg.model.comment;

import com.example.alexander.ufgg.model.interfaces.combos.Combo;
import com.example.alexander.ufgg.model.interfaces.comment.Comment;
import com.example.alexander.ufgg.utils.Item;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/26/16.
 */
public class CommentImpl extends Item implements Comment {

    private int commentId;
    private int comboId;
    private String comment;
    private String postDate;


    public CommentImpl(int id,int comboId, String comment,String postDate){
        this.commentId = id;
        this.comboId = comboId;
        this.comment = comment;
        this.postDate = postDate;
    }
    public CommentImpl(JSONObject jsonObject) throws JSONException {
        this.commentId = jsonObject.getInt("commentId");
        this.comment = jsonObject.getString("comboComment");
        this.postDate = jsonObject.getString("postDate");
    }
    public CommentImpl(int comboId, String comment){
        this.comboId = comboId;
        this.comment = comment;
    }

    public CommentImpl(Combo combo, String comment){
        this.comboId = combo.getComboId();
        this.comment = comment;
    }

    @Override
    public int getCommentId() {
        return this.commentId;
    }

    @Override
    public int getComboId() {
        return this.comboId;
    }

    @Override
    public String getComment() {
        return this.comment;
    }

    @Override
    public String getPostDate() {
        return this.postDate;
    }
}
