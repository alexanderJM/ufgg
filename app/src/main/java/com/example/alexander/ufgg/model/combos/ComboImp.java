package com.example.alexander.ufgg.model.combos;

import com.example.alexander.ufgg.model.interfaces.combos.Combo;
import com.example.alexander.ufgg.utils.Item;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/20/16.
 */
public class ComboImp extends Item implements Combo {
    private int comboId;
    private String combo;
    private String postDate;
    private String damage;
    private String comboCount;
    private String meterCount;
    public ComboImp(int id,String combo,String postDate,String damage, String comboCount, String meterCount){
        this.comboId = id;
        this.combo = combo;
        this.postDate = postDate;
        this.damage = damage;
        this.comboCount = comboCount;
        this.meterCount = meterCount;
    }
    public ComboImp(JSONObject jsonObject) throws JSONException {
        this.comboId = jsonObject.getInt("comboId");
        this.combo = jsonObject.getString("combo");
        this.postDate = jsonObject.getString("postDate");
        this.damage = jsonObject.getString("damage");
        this.comboCount = jsonObject.getString("comboCount");
        this.meterCount = jsonObject.getString("numberOfMeter");
    }
    public ComboImp(String combo,String postDate){
        this.combo = combo;
        this.postDate = postDate;
    }
    public ComboImp(String combo){
        this.combo = combo;
    }

    @Override
    public int getComboId() {
        return this.comboId;
    }

    @Override
    public String getCombo() {
        return this.combo;
    }

    @Override
    public String getPostDate() {
        return this.postDate;
    }

    @Override
    public String getDamage() {
        return this.damage;
    }

    @Override
    public String getComboCount() {
        return this.comboCount;
    }

    @Override
    public String getMeterNumber() {
        return this.meterCount;
    }
}
