package com.example.alexander.ufgg.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.alexander.ufgg.enums.CharacterEnums;
import com.example.alexander.ufgg.joystick.JoyStickMain;
import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.utils.ComboUtils;
import com.example.alexander.ufgg.combo_controls.Control;
import com.example.alexander.ufgg.volley_task.VolleyCombos;

import org.json.JSONException;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/21/16.
 */
public class ComboFragment extends Fragment {
    RelativeLayout layout_joystick;
    JoyStickMain joyStickMain;
    public static int currentMotion;
    ImageButton square,circle,triangle,ex;
    EditText editText;
    Button button;
    public static String combo = "";
    CharacterEnums characterEnums;
    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_combo,container,false);
        characterEnums = (CharacterEnums)getArguments().getSerializable("character");
        layout_joystick = (RelativeLayout)rootView.findViewById(R.id.layout_joystick);
        editText = (EditText)rootView.findViewById(R.id.edittext2);
        joyStickMain = new JoyStickMain(getActivity()
                , layout_joystick, R.drawable.image_button);
        joyStickMain.setStickSize(75, 75);
        joyStickMain.setLayoutSize(250, 250);
        joyStickMain.setLayoutAlpha(75);
        joyStickMain.setStickAlpha(50);
        joyStickMain.setOffset(45);
        joyStickMain.setMinimumDistance(25);
        layout_joystick.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                joyStickMain.drawStick(arg1);
                if (arg1.getAction() == MotionEvent.ACTION_DOWN
                        || arg1.getAction() == MotionEvent.ACTION_MOVE) {

                    int direction = joyStickMain.get8Direction();
                    switch (direction) {
                        case JoyStickMain.STICK_UP:
                            if (currentMotion != JoyStickMain.STICK_UP) {
                                currentMotion = JoyStickMain.STICK_UP;
                                combo = combo + Control.UP.getButtonValue() + "-";
                                SpannableString spannableString = getSpannableStringFromString(combo);
                                editText.setText(spannableString);
                            }
                            break;
                        case JoyStickMain.STICK_UPRIGHT:
                            if (currentMotion != JoyStickMain.STICK_UPRIGHT) {
                                System.out.println("STICK_UPRIGHT");
                                currentMotion = JoyStickMain.STICK_UPRIGHT;
                            }
                            break;
                        case JoyStickMain.STICK_RIGHT:
                            if (currentMotion != JoyStickMain.STICK_RIGHT) {
                                currentMotion = JoyStickMain.STICK_RIGHT;
                                combo = combo + Control.FORWARD.getButtonValue() + "-";
                                SpannableString spannableString = getSpannableStringFromString(combo);
                                editText.setText(spannableString);
                            }
                            break;
                        case JoyStickMain.STICK_DOWNRIGHT:
                            if (currentMotion != JoyStickMain.STICK_DOWNRIGHT) {
                                System.out.println("STICK_DOWNRIGHT");
                                currentMotion = JoyStickMain.STICK_DOWNRIGHT;
                            }
                            break;
                        case JoyStickMain.STICK_DOWN:
                            if (currentMotion != JoyStickMain.STICK_DOWN) {
                                currentMotion = JoyStickMain.STICK_DOWN;
                                combo = combo + Control.DOWN.getButtonValue() + "-";
                                SpannableString spannableString = getSpannableStringFromString(combo);
                                editText.setText(spannableString);

                            }
                            break;
                        case JoyStickMain.STICK_DOWNLEFT:
                            if (currentMotion != JoyStickMain.STICK_DOWNLEFT) {
                                System.out.println("STICK_DOWNLEFT");
                                currentMotion = JoyStickMain.STICK_DOWNLEFT;
                            }
                            break;
                        case JoyStickMain.STICK_LEFT:
                            if (currentMotion != JoyStickMain.STICK_LEFT) {
                                currentMotion = JoyStickMain.STICK_LEFT;
                                combo = combo + Control.BACK.getButtonValue() + "-";
                                SpannableString spannableString = getSpannableStringFromString(combo);
                                editText.setText(spannableString);

                            }
                            break;
                        case JoyStickMain.STICK_UPLEFT:
                            if (currentMotion != JoyStickMain.STICK_UPLEFT) {
                                System.out.println("STICK_UPLEFT");
                                currentMotion = JoyStickMain.STICK_UPLEFT;
                            }
                            break;

                        case JoyStickMain.STICK_NONE:
                            if (currentMotion != JoyStickMain.STICK_NONE) {
                                System.out.println("STICK_NONE");
                                currentMotion = JoyStickMain.STICK_NONE;
                            }
                            break;
                        default:
                            throw new IllegalArgumentException(
                                    "The enum" + direction + " is not valid for the Joystick."
                            );
                    }

                } else if (arg1.getAction() == MotionEvent.ACTION_UP) {
                    currentMotion = JoyStickMain.STICK_NONE;
                }
                return true;
            }

        });
        square = (ImageButton)rootView.findViewById(R.id.imageButtonSquare);
        triangle = (ImageButton)rootView.findViewById(R.id.imageButtonTriangle);
        ex = (ImageButton)rootView.findViewById(R.id.imageButtonEx);
        circle = (ImageButton)rootView.findViewById(R.id.imageButtonCircle);
        ImageButtonsListeners(square, triangle, ex, circle);


        button = (Button) rootView.findViewById(R.id.buttonInsert);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(combo.trim().length()>0){
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setTitle("Inserting Combo");
                    progressDialog.setMessage("Please wait...");
                    progressDialog.show();
                    VolleyCombos volleyBackgroundTask = new VolleyCombos(getActivity());
                    try {
                        volleyBackgroundTask.insertCombo(combo.substring(0, combo.length() - 1), characterEnums);
                        volleyBackgroundTask.setOnComboInsertListener(new VolleyCombos.OnComboInsertListener() {
                            @Override
                            public void onComboClicked() {
                                progressDialog.dismiss();
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    combo = "";
                }
                else{
                    Toast.makeText(getActivity(), "THERE'S NO COMBO TO INSERT", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    private SpannableString getSpannableStringFromString(String combo){
        int[] comboItems = ComboUtils.convertSplit(combo);
        SpannableString spannableString = new SpannableString(combo.replace("-"," "));
        int stringPosition = 0;
        for (int i = 0; i < comboItems.length; i++) {

            Drawable d = getActivity().getResources().getDrawable(Control.getValue(comboItems[i]).getPs4Drawable());
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
            spannableString.setSpan(span, stringPosition, stringPosition + String.valueOf(comboItems[i]).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            stringPosition = stringPosition+String.valueOf(comboItems[i]).length()+1;
        }
        return spannableString;
    }

    private void ImageButtonsListeners(ImageButton square, ImageButton triangle,ImageButton ex, ImageButton circle){



        square.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                combo = combo + Control.ONE.getButtonValue()+"-";
                SpannableString spannableString = getSpannableStringFromString(combo);
                editText.setText(spannableString);
            }
        });

        triangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                combo = combo + Control.TWO.getButtonValue()+"-";
                SpannableString spannableString = getSpannableStringFromString(combo);
                editText.setText(spannableString);
            }
        });

        ex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                combo = combo + Control.THREE.getButtonValue()+"-";
                SpannableString spannableString = getSpannableStringFromString(combo);
                editText.setText(spannableString);
            }
        });

        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                combo = combo + Control.FOUR.getButtonValue()+"-";
                SpannableString spannableString = getSpannableStringFromString(combo);
                editText.setText(spannableString);
            }
        });
    }
}
