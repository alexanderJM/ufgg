package com.example.alexander.ufgg.volley_task;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.alexander.ufgg.enums.CharacterEnums;
import com.example.alexander.ufgg.model.combos.ComboImp;
import com.example.alexander.ufgg.recycler_view.combo_by_character.ComboByCharacterAdapter;
import com.example.alexander.ufgg.utils.Item;
import com.example.alexander.ufgg.utils.MySingleton;
import com.example.alexander.ufgg.utils.OnItemClickListener;
import com.example.alexander.ufgg.utils.OnVolleyResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/21/16.
 */
public class VolleyCombos {
    private static OnVolleyResponse mVolleyResponse;

    public void setVolleyResponse(OnVolleyResponse listener) {

        mVolleyResponse = listener;
    }

    private static OnComboInsertListener mOnComboInsertListener;

    public interface OnComboInsertListener {
        void onComboClicked();
            }
    public void setOnComboInsertListener(OnComboInsertListener listener) {

                        mOnComboInsertListener = listener;
            }
    private Context context;
    private ArrayList<Item> arrayList = new ArrayList<>();
    private String getCombos_url = "http://192.168.1.5:8080/UFGGService/combos/";
    private String insertCombo_url = "http://192.168.1.5:8080/UFGGService/combo/";
    private int mStatusCode = 0;

    public VolleyCombos(Context context){
        this.context = context;
    }

    public void insertCombo(String combo, CharacterEnums characterEnums) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("game", new JSONObject().put("gameId",characterEnums.getGameEnum().getId()));
        json.put("gameCharacter", new JSONObject().put("characterId", characterEnums.getId()));
        json.put("combo", combo);
        System.out.println("JSON " + json);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,insertCombo_url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context, "SUCCESS!", Toast.LENGTH_SHORT).show();
                        mOnComboInsertListener.onComboClicked();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                error.printStackTrace();                    }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                if (response.data == null || response.data.length == 0) {
                    return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                } else {
                    return super.parseNetworkResponse(response);
                }
            }
        };
        MySingleton.getInstance(context).addToRequestQue(req);
    }

    public ArrayList<Item> getComboListWithPagination(final int page) throws InterruptedException {
        String getComboByPagination_url = getCombos_url+page;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, getComboByPagination_url, (String) null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int count = 0;
                        if (mStatusCode == 204) {
                            Toast.makeText(context, "No Comments", Toast.LENGTH_SHORT).show();

                        }else{
                            while (count < response.length()) {

                                try {
                                    JSONObject jsonObject = response.getJSONObject(count);
                                    ComboImp comboImp = new ComboImp(jsonObject);
                                    arrayList.add(comboImp);
                                    count++;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            mVolleyResponse.onResponse(page * 10,arrayList);



                            //jsonCallback.onSuccessGetPagination(page * 10, arrayList);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVolleyResponse.onFailure();
                //jsonCallback.onFailure();
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){
            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                if (response.data == null || response.data.length == 0) {
                    return Response.success(new JSONArray(), HttpHeaderParser.parseCacheHeaders(response));
                } else {
                    return super.parseNetworkResponse(response);
                }            }
        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQue(jsonArrayRequest);
        return arrayList;
    }

    public ArrayList<Item> getComboListByCharacterWithPagination(final int page, CharacterEnums characterEnums) throws InterruptedException {
        String getComboByPagination_url = getCombos_url+page+"/"+characterEnums.getId();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, getComboByPagination_url, (String) null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int count = 0;
                        if (mStatusCode == 204) {
                            Toast.makeText(context, "No Combos", Toast.LENGTH_SHORT).show();

                        }else{
                            while (count < response.length()) {

                                try {
                                    JSONObject jsonObject = response.getJSONObject(count);
                                    ComboImp comboImp = new ComboImp(jsonObject);
                                    arrayList.add(comboImp);
                                    count++;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            mVolleyResponse.onResponse(page * 10,arrayList);
                            //jsonCallback.onSuccessGetPagination(page * 10, arrayList);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVolleyResponse.onFailure();
                //jsonCallback.onFailure();
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){
            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                if (response.data == null || response.data.length == 0) {
                    return Response.success(new JSONArray(), HttpHeaderParser.parseCacheHeaders(response));
                } else {
                    return super.parseNetworkResponse(response);
                }            }
        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQue(jsonArrayRequest);
        return arrayList;
    }
}
