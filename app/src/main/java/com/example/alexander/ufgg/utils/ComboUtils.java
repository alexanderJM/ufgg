package com.example.alexander.ufgg.utils;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/20/16.
 */
public class ComboUtils {

    public static int[] convertSplit(String combo) {

        final String[] comboSplit = combo.split("-");
        final int[] comboInt = new int[comboSplit.length];
        for (int i = 0; i < comboSplit.length; i++) {
            comboInt[i] = Integer.parseInt(comboSplit[i]);
        }
        return comboInt;
    }
}
