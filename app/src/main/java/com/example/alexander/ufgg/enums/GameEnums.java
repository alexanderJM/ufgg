package com.example.alexander.ufgg.enums;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/29/16.
 */
public enum GameEnums {

    MORTAL_KOMBAT_X                                          (1,"Mortal Kombat X");

    private int id;
    private String name;


    GameEnums(int id, String name) {
        this.id = id;
        this.name = name;

    }
    public static GameEnums getValue(int code) {
        for (GameEnums items : GameEnums.values()) {
            if (items.id == code) {
                return items;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }


}
