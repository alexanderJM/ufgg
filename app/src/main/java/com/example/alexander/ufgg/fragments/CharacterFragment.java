package com.example.alexander.ufgg.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.enums.CharacterEnums;
import com.example.alexander.ufgg.enums.GameEnums;
import com.example.alexander.ufgg.recycler_view.character_recycler_view.HeaderNumberedAdapter;
import com.example.alexander.ufgg.recycler_view.character_recycler_view.MarginDecoration;
import com.example.alexander.ufgg.utils.OnItemClickListener;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/28/16.
 */
public class CharacterFragment extends Fragment {
    int characters;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_character,container,false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.addItemDecoration(new MarginDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);

        setCharacterNumber();
        View header = LayoutInflater.from(getActivity()).inflate(R.layout.header, recyclerView, false);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Character Select", Toast.LENGTH_SHORT)
                        .show();
            }
        });

        final HeaderNumberedAdapter adapter = new HeaderNumberedAdapter(header, characters, CharacterEnums.values());
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int pos, Bundle bundle) {
                CharacterOptionsFragment characterOptionsFragment = new CharacterOptionsFragment();
                characterOptionsFragment.setArguments(bundle);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, characterOptionsFragment, "characterOptionsFragment");
                ft.addToBackStack(null).commit();
                /*
                CombosByCharacterFragment combosByCharacterFragment= new CombosByCharacterFragment();
                combosByCharacterFragment.setArguments(bundle);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, combosByCharacterFragment, "CommentFragment");
                ft.commit();*/
            }
        });
        final GridLayoutManager manager = (GridLayoutManager) recyclerView.getLayoutManager();
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return adapter.isHeader(position) ? manager.getSpanCount() : 1;
            }
        });
        recyclerView.setAdapter(adapter);
        return rootView;
    }
    public void setCharacterNumber(){
        characters = CharacterEnums.getEnumsByGame((GameEnums) getArguments().getSerializable("Game")).size();
        System.out.println("NUMERO : " + characters);

    }
}
