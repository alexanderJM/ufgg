package com.example.alexander.ufgg.model.interfaces.comment;

import java.io.Serializable;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/26/16.
 */
public interface Comment extends Serializable {
    /**
     * This method returns the Comment ID.
     * @return
     */
    int getCommentId();
    /**
     * This method returns the Comment's Combo ID.
     * @return
     */
    int getComboId();
    /**
     * This method returns the Combo in an Int value.
     * @return
     */
    String getComment();
    /**
     * This method returns the Combo post date in Epoch.
     * @return
     */
    String getPostDate();
}
