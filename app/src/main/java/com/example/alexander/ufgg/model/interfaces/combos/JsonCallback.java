package com.example.alexander.ufgg.model.interfaces.combos;

import com.example.alexander.ufgg.utils.Item;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 8/22/16.
 */
public interface JsonCallback {
    void onSuccessGet(ArrayList<Item> arrayList);
    void onSuccessInsert(JSONObject jsonObject);
    void onSuccessGetPagination(int position,ArrayList<Item> arrayList);
    void onFailure();



}
