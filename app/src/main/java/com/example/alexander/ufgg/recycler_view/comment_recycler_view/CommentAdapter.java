package com.example.alexander.ufgg.recycler_view.comment_recycler_view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.model.comment.CommentImpl;
import com.example.alexander.ufgg.model.interfaces.comment.Comment;
import com.example.alexander.ufgg.recycler_view.Footer;
import com.example.alexander.ufgg.utils.AnimationUtils;
import com.example.alexander.ufgg.utils.EpochUtils;
import com.example.alexander.ufgg.utils.Item;

import java.util.List;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/26/16.
 */
public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static OnItemClickListener mOnItemClickLister;

    public interface OnItemClickListener {
        void onItemClicked(View view, int pos,Bundle bundle);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {

        mOnItemClickLister = listener;
    }
    private List<Item> data;

    private static final int TYPE_COMMENT = 0;
    private static final int TYPE_FOOTER = 1;
    private Context mContext;
    public CommentAdapter(@NonNull List<Item> data,Context mContext) {
        this.data = data;
        this.mContext =mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof CommentImpl) {
            return TYPE_COMMENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_COMMENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_row, parent, false);
            CommentViewHolder cvh = new CommentViewHolder(row);
            return cvh;
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_footer, parent, false);
            FooterViewHolder vh = new FooterViewHolder(row);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CommentViewHolder) {
            CommentViewHolder commentViewHolder = (CommentViewHolder)holder;
            //commentViewHolder.getComment().setText("");
            //commentViewHolder.getPostDate().setText("");
            Comment commentEntity = (Comment) data.get(position);
            commentViewHolder.getPostDate().setText(EpochUtils.stringEpochToDate(commentEntity.getPostDate()));
            commentViewHolder.getComment().setText(commentEntity.getComment());
            AnimationUtils.animate(holder, false);

        }
        //FOOTER: nothing to do

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class CommentViewHolder extends RecyclerView.ViewHolder {
        private TextView comment, postDate;
        private LinearLayout linearLayout;
        public CommentViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.comment_row_layout);

            comment = (TextView) itemView.findViewById(R.id.comment_row_text);
            postDate = (TextView)itemView.findViewById(R.id.comment_row_date);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

        public TextView getComment() {
            return comment;
        }

        public TextView getPostDate() {
            return postDate;
        }

        public LinearLayout getLinearLayout() {
            return linearLayout;
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar getProgressBar() {
            return progressBar;
        }

        private ProgressBar progressBar;

        public FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.footer);
        }
    }

}
