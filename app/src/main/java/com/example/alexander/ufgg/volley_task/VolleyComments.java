package com.example.alexander.ufgg.volley_task;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.alexander.ufgg.model.comment.CommentImpl;
import com.example.alexander.ufgg.model.interfaces.comment.Comment;
import com.example.alexander.ufgg.utils.Item;
import com.example.alexander.ufgg.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/26/16.
 */
public class VolleyComments {
    private static CallBackVolley mCallBackVolley;

    public interface CallBackVolley {
        void onSuccessGetCommentList(ArrayList<Item> arrayList);
    }
    public void setCallBackVolley(CallBackVolley listener) {

        mCallBackVolley = listener;
    }
    private Context context;
    private ArrayList<Item> arrayList = new ArrayList<>();
    private String getComments_url = "http://192.168.1.5:8080/UFGGService/comments/";
    private String insertComment_url = "http://192.168.1.5:8080/UFGGService/comment/";
    private int mStatusCode = 0;

    public VolleyComments(Context context){
        this.context = context;
    }

    public ArrayList<Item> getCommentList(int comboId) throws InterruptedException {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, getComments_url + 0 + "/" + comboId, (String) null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int count = 0;
                        if (mStatusCode == 204) {
                            Toast.makeText(context, "No Comments", Toast.LENGTH_SHORT).show();

                        } else {
                            while (count < response.length()) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject(count);
                                    CommentImpl comment = new CommentImpl(jsonObject);
                                    System.out.println("COMMENT " + comment.getComment());
                                    arrayList.add(comment);
                                    count++;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            mCallBackVolley.onSuccessGetCommentList(arrayList);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){
            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                if (response.data == null || response.data.length == 0) {
                    return Response.success(new JSONArray(), HttpHeaderParser.parseCacheHeaders(response));
                } else {
                    return super.parseNetworkResponse(response);
                }
            }
        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(context).addToRequestQue(jsonArrayRequest);
        return arrayList;
    }

    public void insertComment(Comment comment, int comboId) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("combo", new JSONObject().put("comboId",comboId));
        json.put("comboComment", comment.getComment());
        System.out.println("JSON " + json);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,insertComment_url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context, "SUCCESS!", Toast.LENGTH_SHORT).show();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                error.printStackTrace();                    }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                mStatusCode = response.statusCode;
                if (response.data == null || response.data.length == 0) {
                    return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                } else {
                    return super.parseNetworkResponse(response);
                }
            }
        };

        MySingleton.getInstance(context).addToRequestQue(req);
    }
}
