package com.example.alexander.ufgg.recycler_view.character_recycler_view;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexander.ufgg.R;
import com.example.alexander.ufgg.enums.CharacterEnums;
import com.example.alexander.ufgg.utils.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Jimenez (alexanderenriquejm@gmail.com) on 9/28/16.
 */
public class HeaderNumberedAdapter extends RecyclerView.Adapter<TextViewHolder> {

    private static OnItemClickListener mOnItemClickLister;

    public void setOnItemClickListener(OnItemClickListener listener) {

        mOnItemClickLister = listener;
    }
    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    private static final int ITEM_VIEW_TYPE_ITEM = 1;

    private final View header;
    private final List<String> labels;
    private CharacterEnums[] enums;
    public HeaderNumberedAdapter(View header, int count ,CharacterEnums[] enums) {
        if (header == null) {
            throw new IllegalArgumentException("header may not be null");
        }
        this.enums =enums;
        this.header = header;
        this.labels = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            labels.add(String.valueOf(i));
        }
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    @Override
    public TextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_HEADER) {
            return new TextViewHolder(header);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.character_row, parent, false);
        return new TextViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TextViewHolder holder, final int position) {
        if (isHeader(position)) {
            return;
        }
        final Integer characterId = Integer.parseInt(labels.get(position -1))+1;  // Subtract 1 for header
        System.out.println();
        holder.imageButton.setImageResource(enums[position -1].getDrawable());
        holder.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //REDO when added a new Game
                Bundle bundle = new Bundle();
                bundle.putSerializable("character", CharacterEnums.getValue(characterId));
                //bundle.putSerializable("game", MKXCharacterEnums.getValue(characterId));

                int position = v.getLayoutDirection();
                mOnItemClickLister.onItemClicked(v, position, bundle);
                //Toast.makeText(
                   //     holder.imageButton.getContext(), ""+characterId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return labels.size() + 1;
    }
}
